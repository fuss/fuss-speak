from threading import Thread
import signal
import os
import subprocess
import tkinter
from tkinter import messagebox
from tkinter import *
from tkinter.ttk import *

window = tkinter.Tk()

window.title("Benvenuti nel lettore FUSS")

window.geometry("440x420")

window.resizable(True, True)



def disable_event():
   pass

#Disable the Close Window Control Icon
window.protocol("WM_DELETE_WINDOW", disable_event)


# Add image file
bg = PhotoImage(file = "/usr/share/fuss-speak/robot-reading.png")
  
canvas1 = Canvas( window, width = 440, height = 420)
  
canvas1.pack(fill = "both", expand = True)
  
# Display image
canvas1.create_image( 0, 0, image = bg, anchor = "nw")

def LanguageonClick():
    messagebox.showinfo('Lingua', "Scegli la lingua di lettura")

button1 = tkinter.Button(window,text = "Lingua", command = LanguageonClick)
button1.pack()
button1.place(x=45,y=20)

lang = StringVar()
lang.set('it-IT')

rad1 = Radiobutton(window,text='de-DE', value='de-DE', variable=lang)

rad2 = Radiobutton(window,text='en-GB', value='en-GB', variable=lang)

rad3 = Radiobutton(window,text='en-US', value='en-US', variable=lang)

rad4 = Radiobutton(window,text='es-ES ', value='es-ES', variable=lang)

rad5 = Radiobutton(window,text='fr-FR  ', value='fr-FR', variable=lang)

rad6 = Radiobutton(window,text=' it-IT  ', value='it-IT', variable=lang)

rad1.place(x=50,y=60)

rad2.place(x=50,y=90)

rad3.place(x=50,y=120)

rad4.place(x=50,y=150)

rad5.place(x=50,y=180)

rad6.place(x=50,y=210)


def SpeedonClick():
    messagebox.showinfo('Velocita', "Scegli la velocita' di lettura")

button2 = tkinter.Button(window,text = "Velocita'", command = SpeedonClick)
button2.pack()
button2.place(x=327.5,y=20)

speed = StringVar()
speed.set(0.8)

rad1 = Radiobutton(window,text='0.6', value='0.6', variable=speed)

rad2 = Radiobutton(window,text='0.8', value='0.8', variable=speed)

rad3 = Radiobutton(window,text='1.0', value='1', variable=speed)

rad4 = Radiobutton(window,text='1.2', value='1.2', variable=speed)

rad5 = Radiobutton(window,text='1.4', value='1.4', variable=speed)

rad6 = Radiobutton(window,text='1,6', value='1.6', variable=speed)



rad1.place(x=350,y=60)

rad2.place(x=350,y=90)

rad3.place(x=350,y=120)

rad4.place(x=350,y=150)

rad5.place(x=350,y=180)

rad6.place(x=350,y=210)


class GUI:
    def __init__(self,master):
        self.master = master
        #master.title('Program')

        #Button to Run command
        self.button_idarun = Button(master,text="Leggi il testo evidenziato",command=self.Runthread)
        self.button_idarun.place(x=40,y=300)
   

    #Function that runs command.
    def Run(self):
        self.p = subprocess.call(['bash', '/usr/share/fuss-speak/play.sh', lang.get(), speed.get()])
        

    #Function that calls Run in a thread, so other buttons can still be pressed ...
    def Runthread(self):
        self.thread = Thread(target=self.Run)
        self.thread.start()

def playbreak():
    subprocess.run(['pkill', 'play'])

stop = Button(window, text="Interrompi la lettura", command=playbreak)

stop.pack()

stop.place(x=55,y=360)

def playitslowlyonclick():
	subprocess.call(['bash','/usr/share/fuss-speak/playitslowly.sh',lang.get(),speed.get()])
	
btn3 = Button(window, text="Apri in app audio", command=playitslowlyonclick)

btn3.place(x=300,y=300)



def close_win():
    window.destroy()

def closeandbreak():
    close_win()
    playbreak()

    
#Create a button to break and close the window
button0 = Button(window, text ="Esci", command = closeandbreak)
button0.pack()
button0.place(x=315,y=360)


photo = PhotoImage(file = "/usr/share/fuss-speak/fuss-speak.png")
window.iconphoto(False, photo)

gui = GUI(window)

window.mainloop()




